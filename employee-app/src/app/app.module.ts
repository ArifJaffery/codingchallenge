import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule} from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';


import { AppComponent } from './app.component';
import { LoginComponent } from './components/login-component/login-component.component';
import { HeaderComponentComponent } from './components/header-component/header-component.component';
import { EmployeeComponent } from './components/employee-component/employee-component.component';
import { LoginService} from './services/login-service.service';
import { DataService} from './services/data-service.service';
import { AuthorizationService} from './services/authorization-service.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponentComponent,
    EmployeeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    HttpClientInMemoryWebApiModule.forRoot(DataService, { dataEncapsulation: false })    
  ],
  providers: [LoginService, AuthorizationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
