import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import { LoginService } from '../services/login-service.service'

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService implements CanActivate {
  constructor( private loginService: LoginService) { 
  }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean>{
    const promise = new Promise<boolean>((resolve,reject) => {
      if (this.loginService) {
        if (this.loginService.state) {
          resolve(this.loginService.state.valid);
        } else { 
          resolve(false);
        }
      } else {
        reject(false);
      }
    });
    return promise;
  }
}
