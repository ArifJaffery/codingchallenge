import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { iuser} from '../api/api';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  baseUrl = 'api/';
  dbContext : {
    users: iuser[];
  };

  constructor(private http: HttpClient) { 

  }

  getUsers(entity: string): Observable<iuser[]> {
    return this.http.get<iuser[]>(this.baseUrl + entity )  }
}
