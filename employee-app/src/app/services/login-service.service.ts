import { Injectable , Inject } from '@angular/core';
import { iuser} from '../api/api';
import { users} from '../data-store/users.json';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  url = 'api/users';  
  state: {
    user: iuser;
    valid: boolean;
    message: string;
  } ; 

  constructor(private httpClient: HttpClient,private router: Router ) { 
    this.state = { user: undefined, valid: false, message: undefined};
  }

  validateLogin(user: iuser) {
    return (usersObservable: Observable<iuser[]>): Observable<iuser[]> => {
      if (usersObservable) {
        usersObservable.subscribe(users => {
          if (Array.isArray(users)){
            if (users.length > 0){
              const findUser= users.find(u => u.userName === user.userName && u.password === user.password);
              if (findUser){
                this.state  = {user: user, valid : true, message: undefined};    
                this.router.navigate(['/employees']);           
              }else {
                this.state  = {user: undefined, valid : false, message: 'Please enter correct user/password'};               

              }
              console.log(this.state);
            }            
          }
        });
        return usersObservable;
      }else {
        return of([]);
      }
    };
  }

  login(user: iuser) {

    let headers = new HttpHeaders();
    headers.append('Authorization','Basic ' + btoa(user.userName + ':' + user.password));

    this.httpClient.get<iuser[]>(this.url, {headers: headers})
        .pipe(
          this.validateLogin(user)
      );
  }    
  log(message: string) {
  }
  handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); 
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

}


  // getUsers (): Observable<iuser[]> {
  //   return this.httpClient.get<iuser[]>(this.url)
  //     .pipe(
  //       catchError(this.handleError<iuser[]>('geUsers', []))
  //     );
  // }

