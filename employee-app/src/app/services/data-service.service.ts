import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { iuser,iemployee} from '../api/api';

import { users} from '../data-store/users.json';
import { employees} from '../data-store/employee.json';

@Injectable({
  providedIn: 'root'
})
export class DataService implements InMemoryDbService  {
  state: {
    users : iuser[];
    employees: iemployee[];
  }

  constructor() { 
    this.state = {
      users: users,
      employees: employees
    };

  }
  createDb() {
    return {...this.state};
  }
}
