import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login-component/login-component.component';
import { EmployeeComponent} from './components/employee-component/employee-component.component';
import { AuthorizationService} from './services/authorization-service.service'; 
const routes: Routes = [
  { path: 'employees', component: EmployeeComponent, canActivate: [AuthorizationService]},
  { path: 'login', component: LoginComponent},
  { path: '', redirectTo : '/login', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
