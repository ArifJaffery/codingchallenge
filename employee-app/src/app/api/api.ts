
export interface iuser {
    userName: string;
    password: string;
}

export interface iemployee {
    id: number;
    firstName: string;
    lastName: string;
    salary: number;
    joiningDate: Date;
    department?: string;
}

