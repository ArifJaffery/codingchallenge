import { iemployee} from '../api/api';

export const employees : iemployee[] =[
    { id: 1, firstName: 'Jonathan', lastName: 'Javier', salary: 120000,joiningDate: new Date('01/10/2000'), department: 'Admin'},
    { id: 1, firstName: 'Sam', lastName: 'chan', salary: 130000,joiningDate: new Date('01/10/2000'), department: 'Admin'},
    { id: 1, firstName: 'Charles', lastName: 'chang', salary: 14320000,joiningDate: new Date('01/10/2000'), department: 'Admin'},
    { id: 1, firstName: 'David', lastName: 'Wu', salary: 120043400,joiningDate: new Date('01/10/2000'), department: 'Admin'},
    { id: 1, firstName: 'Edward', lastName: 'collins', salary: 12430000,joiningDate: new Date('01/10/2000'), department: 'Admin'},
    { id: 1, firstName: 'Philip', lastName: 'Mamoo', salary: 1200343400,joiningDate: new Date('01/10/2000'), department: 'Admin'},
    { id: 1, firstName: 'Steve', lastName: 'Smith', salary: 120434000,joiningDate: new Date('01/10/2000'), department: 'Admin'},
    { id: 1, firstName: 'Nathan', lastName: 'Breelee', salary: 12043000,joiningDate: new Date('01/10/2000'), department: 'Admin'},
    { id: 1, firstName: 'Richard', lastName: 'Fracaro', salary: 12043000,joiningDate: new Date('01/10/2000'), department: 'Admin'},
    { id: 1, firstName: 'Michal', lastName: 'Jackson', salary: 12043000,joiningDate: new Date('01/10/2000'), department: 'Admin'},
    { id: 1, firstName: 'Jordan', lastName: 'Lee', salary: 120000,joiningDate: new Date('01/10/2000'), department: 'Admin'},
    { id: 1, firstName: 'Lee', lastName: 'Choo', salary: 120000,joiningDate: new Date('01/10/2000'), department: 'Admin'},
    { id: 1, firstName: 'Bush', lastName: 'Junir', salary: 120000,joiningDate: new Date('01/10/2000'), department: 'Admin'},
    { id: 1, firstName: 'Kennedy', lastName: 'Comedy', salary: 120000,joiningDate: new Date('01/10/2000'), department: 'Admin'},
    { id: 1, firstName: 'Carlos', lastName: 'Alkantara', salary: 120000,joiningDate: new Date('01/10/2000'), department: 'Admin'},
    { id: 1, firstName: 'Mario', lastName: 'Aks', salary: 120000,joiningDate: new Date('01/10/2000'), department: 'Admin'},
    { id: 1, firstName: 'Anthony', lastName: 'Malik', salary: 120000,joiningDate: new Date('01/10/2000'), department: 'Admin'},
    { id: 1, firstName: 'Aldo', lastName: 'Bararra', salary: 120000,joiningDate: new Date('01/10/2000'), department: 'Admin'},
    { id: 1, firstName: 'Ceaser', lastName: 'Judy', salary: 120000,joiningDate: new Date('01/10/2000'), department: 'Admin'},
    { id: 1, firstName: 'Naejal', lastName: 'micahel', salary: 120000,joiningDate: new Date('01/10/2000'), department: 'Admin'},
    { id: 1, firstName: 'Peter', lastName: 'Jackson', salary: 120000,joiningDate: new Date('01/10/2000'), department: 'Admin'},

] ;