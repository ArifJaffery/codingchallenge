import { Component, OnInit } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { iemployee, iuser } from '../../api/api';

@Component({
  selector: 'app-employee-component',
  templateUrl: './employee-component.component.html',
  styleUrls: ['./employee-component.component.css']
})
export class EmployeeComponent implements OnInit {
  url ='api/employees';
  state: {
    employees: iemployee[]
  };
  constructor(private httpClient: HttpClient) { 
    this.state = { employees: undefined};
  }
  ngOnInit() {
    this.httpClient.get<iemployee[]>(this.url)
      .subscribe(response => {
        console.log(response);
        this.state = { employees : response};
      });
  }
}
