import { Component, OnInit } from '@angular/core';
import { LoginService} from '../../services/login-service.service';
import { FormGroup, Validators, ValidatorFn, ValidationErrors } from '@angular/forms';
import { iuser } from 'src/app/api/api';
import { FormBuilder } from '@angular/forms';

export const userValidator = (): ValidatorFn => {
  return (formGroup: FormGroup): ValidationErrors => {
    return null; 
  }
}

@Component({
  selector: 'app-login-component',
  templateUrl: './login-component.component.html',
  styleUrls: ['./login-component.component.css']
})
export class LoginComponent implements OnInit {
  formGroup: FormGroup;
  constructor( private loginService: LoginService, private fb: FormBuilder) { 
    this.formGroup = this.fb.group({
      userName: ['', Validators.required],
      password: ['', Validators.required]
    });

    console.log(this.formGroup);
  }

  ngOnInit() {
    // const user: iuser = {userName: 'guest', password: '123'};
    // this.loginService.login(user).subscribe(response => console.log(response));
  }
  isformPistine(formGroup: FormGroup) {
    const { userName, password} = formGroup.controls;
    if ((userName.pristine === true) && (password.pristine === true)){
      console.log('Pistine');
      return true;
    } else {
      return false;
    }
  }
  loginMessage () {
    return this.loginService.state.message;
  }
  login (user: iuser) {
    this.loginService.login(user);
  }
  isValidFormGroup () {
    if (this.formGroup){
      return this.formGroup.valid;
    }
    return false;
  }
}
