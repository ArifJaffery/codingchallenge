import { Component, OnInit } from '@angular/core';
import { LoginService} from '../../services/login-service.service';

@Component({
  selector: 'app-header-component',
  templateUrl: './header-component.component.html',
  styleUrls: ['./header-component.component.css']
})
export class HeaderComponentComponent implements OnInit {
  constructor(private loginService: LoginService) { }

  ngOnInit() {
  
  }

  getCurrentUser() {
    if (this.loginService.state) {
      const {user, valid} = this.loginService.state;
      if (user && valid){
        if (valid === true){
          return user.userName;        }
      }
    }

    return undefined;
  }

}
