// require('dotenv').config();
// const createError = require('http-errors');
const express = require('express');
// const path = require('path');
// const cookieParser = require('cookie-parser');
// const logger = require('morgan');

// Endpoints
// const usersRouter = require('./routes/users');
// const listsRouter = require('./routes/lists');
// const listItemsRouter = require('./routes/list-items');

// Init app
const app = express();

// Middleware
// app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
// app.use(cookieParser());
// app.use(express.static(path.join(__dirname, 'public')));

// Use endpoint routers
// app.use('/users', usersRouter);
// app.use('/lists', listsRouter);
// app.use('/lists', listItemsRouter);

// Catch 404 and forward to error handler
// app.use(function(req, res, next) {
//   next(createError(404));
// });

// Error handler
// app.use(function(err, req, res, next) {
//   console.error(err);
//   res.status(err.status || 500).json({
//     error: err
//   });
// });

module.exports = app;
